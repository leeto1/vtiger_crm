<?php

class Contacts_ChangeRate_View extends Vtiger_Index_View
{
    /**
     * Undocumented function
     *
     * @param Vtiger_Request $request
     *
     * @return void
     */
    public function process(Vtiger_Request $request)
    {
        $module = Vtiger_Module_Model::getInstance('Contacts');
        $record = $request->get('record');
        
        $viewer = $this->getViewer($request);
        $viewer->assign("RECORD",$record);
        
        echo $viewer->view('ChangeRate.tpl', 'Contacts');
    }
}
