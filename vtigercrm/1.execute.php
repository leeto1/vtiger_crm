<?php

require_once 'vtlib/Vtiger/Module.php';
require_once 'include/utils/utils.php';
require_once 'modules/com_vtiger_workflow/VTEntityMethodManager.inc';


global $adb;

$emm = new VTEntityMethodManager($adb);
$emm->addEntityMethod("Contacts","Update Contact Rate","modules/ContactRating/update_contact_rate.php","updateContactRate");

$adb->query("INSERT INTO `vtiger_app2tab` (`tabid`, `appname`, `sequence`, `visible`) VALUES (50, 'TOOLS', 6, 1)");
		

