<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class ContactRating_Record_Model extends Vtiger_Record_Model {
    
    function contactExist($contact_id){ 
        global $adb;
        $result = $adb->pquery("SELECT * FROM vtiger_contactrating WHERE contactid=?",array($contact_id));
        $num_rows = $adb->num_rows($result);
        if($num_rows > 0){
            return true;
        }
        return false;
    }

    function updateRate($rate,$contact_id){
        global $adb;
        $adb->pquery("UPDATE vtiger_contactrating SET rating=? WHERE contactid =?",array($rate,$contact_id)); 
        return true;
    }

    function createRate($reference,$contact_id,$rate){
        $rate_model = Vtiger_Record_Model::getCleanInstance('ContactRating');
        $rate_model->set('reference',$reference);
        $rate_model->set('contactid',$contact_id);
        $rate_model->set('rating',$rate);
        $rate_model->save();
        return true;
    }

}