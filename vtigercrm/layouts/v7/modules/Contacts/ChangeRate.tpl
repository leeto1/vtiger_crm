{strip}
    <div id="addFolderContainer" class="modal-dialog" style='min-width:500px;'>
        <div class='modal-content'>
            {include file="ModalHeader.tpl"|vtemplate_path:$MODULE TITLE={vtranslate('LBL_CHANGE_RATE')}}
                <div class="modal-body">
                	<div class="mb-3 row" style="padding-top: 10px;">
				    	<label for="staticEmail" class="col-sm-4 col-form-label">{vtranslate($MODULE)} {vtranslate("Rate Number")}</label>
				    	<div class="col-sm-8">
				      		<select class="select2" id="rating">
				      			{for $foo=1 to 10}
				      				<option value="{$foo}">{$foo}</option>
				      			{/for}
				      		</select>
				   	 	</div>
					</div>
                </div>
                <div class="modal-footer ">
		        <center>
		            {if $BUTTON_NAME neq null}
		                {assign var=BUTTON_LABEL value=$BUTTON_NAME}
		            {else}
		                {assign var=BUTTON_LABEL value={vtranslate('LBL_SAVE', $MODULE)}}
		            {/if}
		            <button {if $BUTTON_ID neq null} id="{$BUTTON_ID}" {/if} class="btn btn-success" data-id="{$RECORD}" id="changeRate"><strong>{$BUTTON_LABEL}</strong></button>
		            <a href="#" class="cancelLink" type="reset" data-dismiss="modal">{vtranslate('LBL_CANCEL', $MODULE)}</a>
		        </center>
			</div>
        </div>
    </div>
{/strip}