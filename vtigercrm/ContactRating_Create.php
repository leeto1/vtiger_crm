<?php
include_once('vtlib/Vtiger/Module.php');

$moduleInstance = new Vtiger_Module();
$moduleInstance->name = "ContactRating";
$moduleInstance->parent = 'Tools';
$moduleInstance->save();
$moduleInstance->initTables();

$blockInstance = new Vtiger_Block();
$blockInstance->label = 'LBL_ContactRating_INFORMATION';
$moduleInstance->addBlock($blockInstance);

$fn_field = new Vtiger_Field();
$fn_field->name = 'reference';
$fn_field->label = 'LBL_Reference_NAME';
$fn_field->table = 'vtiger_contactrating';
$fn_field->column = 'reference';
$fn_field->columntype = 'VARCHAR(100)';
$fn_field->uitype = 1;
$fn_field->typeofdata = 'V~M';
$fn_field->summaryfield = 1;
$moduleInstance->setEntityIdentifier($fn_field);
$blockInstance->addField($fn_field);


$mentor_field = new Vtiger_Field();
$mentor_field->name = 'contactid';
$mentor_field->label = 'LBL_contactid';
$mentor_field->table = 'vtiger_contactrating';
$mentor_field->column = 'contactid';
$mentor_field->uitype = 10;
$mentor_field->typeofdata = 'V~M';
$mentor_field->summaryfield = 1;
$blockInstance->addField($mentor_field);
$mentor_field->setRelatedModules(array('Contacts'));

$ln_field = new Vtiger_Field();
$ln_field->name = 'rating';
$ln_field->label = 'LBL_RATING';
$ln_field->table = 'vtiger_contactrating';
$ln_field->column = 'rating';
$ln_field->uitype = 7;
$ln_field->typeofdata = 'V~M';
$ln_field->summaryfield = 1;
$blockInstance->addField($ln_field);


// Filter Setup
$filter1 = new Vtiger_Filter();
$filter1->name = 'All';
$filter1->isdefault = true;
$moduleInstance->addFilter($filter1);
$filter1->addField($fn_field)->addField($mentor_field, 1)->addField($ln_field, 2);

// Sharing Access Setup
$moduleInstance->setDefaultSharing();
// Webservice Setup
$moduleInstance->initWebservice();

echo "OK\n";

