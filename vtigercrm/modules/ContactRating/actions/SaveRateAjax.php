<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

include_once 'modules\Contacts\models\Record.php';
include_once 'modules\ContactRating\models\Record.php';

class ContactRating_SaveRateAjax_Action extends Vtiger_Action_Controller {

    public function process(Vtiger_Request $request){
        global $adb;

        $update_rate = new ContactRating_Record_Model();
        $record = $request->get('record');
        $rate = $request->get('rate');

        $record_model = Contacts_Record_Model::getInstanceById($record,'Contacts');
        $fields = $record_model->getData();

        $reference = $fields['firstname'] . ' ' . $fields['lastname'];

        if($update_rate->contactExist($record)){
            $update_rate->updateRate($rate,$record);
        }else{
            $update_rate->createRate($reference,$record,$rate);
        }

        $response = new Vtiger_Response();
        $response->setEmitType(Vtiger_Response::$EMIT_JSON);
        $response->setResult(true);
        $response->emit();
    }
}