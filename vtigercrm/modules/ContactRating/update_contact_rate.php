<?php

require_once 'include/utils/CommonUtils.php';
require_once 'include/utils/VTCacheUtils.php';
require_once('vtlib/Vtiger/Functions.php');
require_once 'modules\ContactRating\models\Record.php';

function updateContactRate($entityData) {
	global $adb;	
	
	$contact_rate_model = new ContactRating_Record_Model();
	$instance_id = explode('x', $entityData->id);
	$contact_id = $instance_id[1];

	$preferred_fields = array('phone',
							  'mobile',
							  'homephone',
							  'department',
							  'fax',
							  'assistantphone',
							  'secondaryemail',
							  'email',
							  'mailingcity',
							  'otherstreet');

	$rate=0;
	foreach($preferred_fields as $key => $preference){
		if($entityData->data[$preference] !== ""){
			$rate++;
		}
	}

	$reference = $entityData->data['firstname']. ' ' . $entityData->data['lastname'];

	if($contact_rate_model->contactExist($contact_id)){
		$contact_rate_model->updateRate($rate,$contact_id);
	}else{
		$contact_rate_model->createRate($reference,$contact_id,$rate);
	}
}
?>